#ifndef SCR_EVENTS_H
#define SCR_EVENTS_H

#include <unordered_map>

#include "scr_event_types.h"

namespace scr {
/**
 * @brief Common event structure. Not all fields always required
 *
 */

typedef void (*scr_callback)(void *);

struct Event {
  Event()
      : type(SCR_EVENT_NONE), id(-1), sub_id(-1), topic(NULL), payload(NULL){};
  // Event(int type, bool active) {
  //   this->type = type;
  //   this->active(active);
  // };
  int type; // required
  int id;
  int sub_id;
  char *topic;
  char *payload;

  bool active() { return this->type & SCR_EVENT_ACTIVE; }
  bool active(bool setval) {
    if (setval)
      this->type |= SCR_EVENT_ACTIVE;
    else
      this->type &= ~SCR_EVENT_ACTIVE;

    return this->active();
  };

  int base_type() { return this->type & ~SCR_EVENT_ACTIVE; };
  int major() { return this->type & SCR_EVENT_MAJOR; };
  static int major(int type) { return type & SCR_EVENT_MAJOR; }

  void release(bool doTopic = true, bool doPayload = true) {
    if (doTopic && topic != NULL) {
      delete[] topic;
      topic = NULL;
    }
    if (doPayload && payload != NULL) {
      delete[] payload;
      payload = NULL;
    }
  }
};

/**
 * @brief list of callbacks depending on the type of event
 * there can only be one callback function for every major type of event
 *
 */
struct Event_Callbacks {
private:
  int _type_major;

public:
  Event_Callbacks() : _type_major(SCR_EVENT_NONE){};

  std::unordered_map<int, scr_callback> _callbacks;

  bool operator[](scr::Event *event) {
    _type_major = event->major();
    auto found = _callbacks.find(_type_major);
    if (found != _callbacks.end()) {
      // if there was a callback associated with this type of event, call
      // it second returns the value of the iterator, which is a function
      // callback ESP_LOGI(TAG, "Found callback TYPE %x",
      // system_event->type);

      //   if (event->topic != NULL) {
      //     ESP_LOGI(TAG, "Callback type %x, Topic %s, Payload %s",
      //     event->type,
      //              event->topic, event->payload);
      //   }

      found->second(event);
      _type_major = SCR_EVENT_NONE;
      return true;
    }

    return false;
  }

  scr::Event_Callbacks &operator()(int type) {
    _type_major = Event::major(type);
    return *this;
  }

  bool operator<<(scr_callback action) {
    if (_type_major == SCR_EVENT_NONE)
      return false;
    _callbacks[_type_major] = action;
    _type_major = SCR_EVENT_NONE;
    return true;
  }
};

} // namespace scr
#endif