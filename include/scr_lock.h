#ifndef SCR_LOCK_H
#define SCR_LOCK_H

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include <stddef.h>

namespace scr {

class Lock {

  SemaphoreHandle_t _sem;
  bool valid;

public:
  Lock() {
    this->_sem = xSemaphoreCreateBinary();
    valid = (this->_sem != NULL);
    if (valid)
      xSemaphoreGive(this->_sem);
  };

  ~Lock(){
    if (valid)
    vSemaphoreDelete(this->_sem);
    valid = false;
  };

  bool lock() {
    if (!valid)
      return false;
    bool ok = xSemaphoreTake(this->_sem, pdMS_TO_TICKS(1));
    return ok;
  };

  bool unlock() {
    if (!valid)
      return false;
    bool ok = xSemaphoreGive(this->_sem);
    return ok;
  };

};

/**
 * @brief while object is in scope, acquires lock. 
 * Automatically releases in destructor (object out of scope)
 * 
 */
class ScopeLock {
  scr::Lock* _lock;
  bool _locked;

public:
  ScopeLock(scr::Lock *lock) : _lock(lock) { _locked = _lock->lock(); };
  ~ScopeLock() { _lock->unlock(); }
  bool locked(){ return this->_locked;}
};
} // namespace scr

#endif