#ifndef SCR_QUEUE_H
#define SCR_QUEUE_H

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

namespace scr {

template <class T> class Queue {
  xQueueHandle _queue;
  TickType_t _wait;
  bool _from_isr;

public:
  Queue(int length, int wait_limit = 0) {
    this->_queue = xQueueCreate(length, sizeof(T));

    this->_wait = (wait_limit == 0) ? portMAX_DELAY : (TickType_t)wait_limit;
    _from_isr = false;
  }

  Queue() {
    this->_queue = NULL;
    this->_wait = portMAX_DELAY;
    _from_isr = false;
  }

  void set(xQueueHandle queue) { this->_queue = queue; }
  void release() {
    vQueueDelete(this->_queue);
    this->_queue = NULL;
  };

  /**
   * @brief marks next queue insertion operation as from ISR
   *
   * @param tElement
   * @return scr::Queue<T>*
   */
  scr::Queue<T> &operator!() {
    _from_isr = true;
    return *this;
  };

  /**
   * @brief inserts an element of type T into the queue
   *
   * @param tElement
   * @return true
   * @return false
   */
  bool operator<<(T *tElement) {
    bool ok = false;
    if (_from_isr)
      ok = xQueueSendFromISR(this->_queue, tElement, (TickType_t)0);
    else
      ok = xQueueSend(this->_queue, tElement, this->_wait);
    _from_isr = false;
    return ok;
  };

  /**
   * @brief extracts an element of type T from the queue
   *
   * @param tElement
   * @return true
   * @return false
   */
  bool operator>>(T *tElement) {
    bool ok = xQueueReceive(this->_queue, tElement, this->_wait);
    return ok;
  }
};
} // namespace scr
#endif