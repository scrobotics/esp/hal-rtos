#ifndef SCR_RING_H
#define SCR_RING_H

#include "freertos/FreeRTOS.h"
#include "freertos/ringbuf.h"

namespace scr {

struct RingItem {
  char *buffer;
  size_t length;

  RingItem() : buffer(NULL), length(0) {}
  RingItem(char *buffer, size_t length) : buffer(buffer), length(length) {}
  bool valid() { return buffer != NULL; }
};

class Ring {

  RingbufHandle_t ring_;
  TickType_t wait_;

public:
  Ring(int size, int wait_limit = 0, RingbufferType_t = RINGBUF_TYPE_NOSPLIT) {
    this->ring_ = xRingbufferCreate(size, RINGBUF_TYPE_NOSPLIT);

    this->wait_ = (wait_limit == 0) ? portMAX_DELAY : (TickType_t)wait_limit;
  }

  Ring() {
    this->ring_ = NULL;
    this->wait_ = portMAX_DELAY;
  }

  bool operator<<(RingItem ri) {
    // Send an item
    UBaseType_t res = xRingbufferSend(this->ring_, ri.buffer, ri.length,
                                      pdMS_TO_TICKS(wait_));
    return (res == pdTRUE);
  }

  bool operator>>(RingItem& ri) {

    // Receive an item from no-split ring buffer

    ri.buffer = (char *)xRingbufferReceive(this->ring_, &ri.length,
                                           pdMS_TO_TICKS(wait_));

    return (ri.buffer != NULL);
  }

  void operator-=(RingItem ri) {
    vRingbufferReturnItem(this->ring_, (void *)ri.buffer);
  }
};
} // namespace scr

#endif
