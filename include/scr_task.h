#pragma once

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "stdio.h"

#define SCR_MAX_TASKS_PER_OBJECT 4

namespace scr {

class TIME {
public:
  static void delay(uint32_t ms) { vTaskDelay(pdMS_TO_TICKS(ms)); }
};

template <class T> using Task_FX = void (T::*)(void);

template <class T> class Runnable {
public:
  Task_FX<T> _fx;
  T *_context;
  TaskHandle_t _handle;

  Runnable() : _handle(NULL) {}
};

template <class T> class Task {
  T *_context;
  Runnable<T> _runnables[SCR_MAX_TASKS_PER_OBJECT];

  static void execute(void *args) {
    Runnable<T> *runnable = static_cast<Runnable<T> *>(args);

    T *that = runnable->_context;
    Task_FX<T> fx = runnable->_fx;
    if (that != NULL)
      (that->*fx)();
    // once it's finished running, we don't need the context object anymore
    TaskHandle_t handle = runnable->_handle;
    runnable->_handle = NULL;
    vTaskDelete(handle);
  };

public:
  Task(){};
  Task(T *context) { this->_context = context; }
  void assign(T *context) { this->_context = context; }

  bool spawn(Task_FX<T> fx, uint32_t stackDepth = 4096, int priority = 1,
             const char *task_name = NULL) {
    int ix_task;
    Runnable<T> *r;
    for (ix_task = 0; ix_task < SCR_MAX_TASKS_PER_OBJECT; ++ix_task) {
      r = &_runnables[ix_task];
      if (r->_handle == NULL)
        break;
    }
    if (ix_task == SCR_MAX_TASKS_PER_OBJECT)
      return false;

    r->_context = this->_context;
    r->_fx = fx;
    void *args = (void *)(r);
    bool ok = true;
    if (task_name)
      ok = xTaskCreate(&Task<T>::execute, task_name, stackDepth, args, priority,
                       &(r->_handle));
    else
      ok = xTaskCreate(&Task<T>::execute, "scr_task", stackDepth, args,
                       priority, &(r->_handle));
    return ok;
  }

  /**
   * @brief Should only be called from parent process
   *
   */
  void killAll() {

    for (int ix = 0; ix < SCR_MAX_TASKS_PER_OBJECT; ++ix) {
      if (_runnables[ix]._handle) {
        vTaskDelete(_runnables[ix]._handle);
        _runnables[ix]._handle = NULL;
      }
    }
  }
};

typedef void (*fx)(void *);

class Fx_Task {
  TaskHandle_t _handle;

public:
  bool spawn(scr::fx fx, void *args = NULL, uint32_t stackDepth = 4096,
             int priority = 1, const char *task_name = "scr_task") {
    if (_handle != NULL)
      return false;
    bool ok = xTaskCreate(fx, task_name, stackDepth, args, priority, &_handle);
    return ok;
  }

  void kill() {
    vTaskDelete(_handle);
    _handle = NULL;
  }
};
} // namespace scr