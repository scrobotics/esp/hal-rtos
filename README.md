# FreeRTOS helper wrappers by scr

Collection of wrapper classes and functions for working with common FreeRTOS structures, in particular:
- Queues
- Mutex Locks/ binary semaphores
- Tasks
- Events and callbacks

Background and more information in our blog: 

## Installation

The code is designed as an esp idf component, and can be installed as a submodule:

`git submodule add https://gitlab.com/scrobotics/scr-hal-rtos/`

Or included as a dependency in a platform.io project, by including the following line in the platformio.ini file:

`lib_deps = https://gitlab.com/scrobotics/scr-hal-rtos/`

All relevant headers are in the `include` subfolder.

## Usage
**Mutex Lock**

```
    scr::Lock lock;
    
    void regularFunction(){

        // other non exclusive code

        bool locked = lock.lock();
        if (locked){
            // run relevant critical section code

            lock.unlock();
        }
    }

    void mutexFunction(){
        scr::ScopeLock key(&lock);      
        // while the 'key' object is in scope , the lock will be secured
        if (key.locked()){
            // run relevant critical section code
        }
        // once the function returns and key goes out of scope, the destructor 
        // automatically releases the
    }

```

**Tasks**

```
// Define an object 'MyWorker' with a function member 'loop' you wish run in a separate thread

class MyWorker {
    int internal_state_;

    void loop(){
        while(1){
            ESP_LOGI("Thread", "My internal state variable is now %d", this->internal_state_);
            this->internal_state_ ++;
            vTaskDelay(1000);
        }
        
    }
};

MyWorker cool_object;

// Template requires the class name as a parameter.
// Constructor takes the actual object on whose context the function will be run

scr::Task<MyWorker> worker(cool_object);
worker.spawn(&MyThread::loop);      
// the loop function will start running on the 'cool_object' object

```


**Events**
```
// callback function
void handler(void* args){
    scr::Event* event = (scr::Event*) args;
    // actual handler code
}

int SYSTEM_EVENT_CODE = 22;

scr::Event_Callbacks callbacks;

scr::Event system_event;
system_event.type = AN_EVENT_CODE;     
callbacks(system) << handler;

// later on, run the callback function

callbacks[system_event.type];

```

**Queues**
```
// Declare queue of pointers to event, 10 elements
scr::Queue<scr::Event*> queue(10);

// Send event to queue
scr::Event event;
queue << &event;

// consume in a loop
while(true){
    scr::Event event;
    bool ok = queue >> &event;
    if (of){
        // perform relevant tasks with queued event

    }

}

```

## License
Copyright 2020 SC Robotics (Sparks, Circuits and Robotics SL)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.